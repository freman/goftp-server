// Copyright 2018 The goftp Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package server

import (
	"crypto/subtle"
)

// Auth is an interface to auth your ftp user login.
type Auth interface {
	CheckPasswd(string, string) (bool, error)
}

// AuthFactory lets you have an authentication factory to match the driver factory.
type AuthFactory interface {
	NewAuth(*Conn) (Auth, error)
}

var (
	_ Auth        = &SimpleAuth{}
	_ AuthFactory = &SimpleAuthFactory{&SimpleAuth{}}
)

// SimpleAuth implements Auth interface to provide a memory user login auth
type SimpleAuth struct {
	Name     string
	Password string
}

// CheckPasswd will check user's password
func (a *SimpleAuth) CheckPasswd(name, pass string) (bool, error) {
	return constantTimeEquals(name, a.Name) && constantTimeEquals(pass, a.Password), nil
}

func constantTimeEquals(a, b string) bool {
	return len(a) == len(b) && subtle.ConstantTimeCompare([]byte(a), []byte(b)) == 1
}

// SimpleAuthFactory wraps a SimpleAuth
type SimpleAuthFactory struct {
	Auth
}

// NewAuth returns the wrapped SimpleAuth
func (s SimpleAuthFactory) NewAuth(_ *Conn) (Auth, error) {
	return s.Auth, nil
}
